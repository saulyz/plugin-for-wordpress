function ScrollAwareSectionModule(el) {
	this._el = el;

	this.init();
}

ScrollAwareSectionModule.prototype = (function() {
	function init() {
		this._el.setAttribute("style", "transition: opacity 500ms ease-out");
		this.onScroll();
	}

	function scrollHandler() {
		let _bounding = this._el.getBoundingClientRect();
		if (
			_bounding.top >= 0 &&
			_bounding.left >= 0 &&
			_bounding.right <=
				(window.innerWidth || document.documentElement.clientWidth) &&
			_bounding.bottom <=
				(window.innerHeight || document.documentElement.clientHeight)
		) {
			_styleVisible.call(this);
		} else {
			_styleInvisible.call(this);
		}
	}

	function _styleVisible() {
		this._el.style.opacity = 1;
	}

	function _styleInvisible() {
		this._el.style.opacity = 0;
	}

	return {
		init: init,
		onScroll: scrollHandler
	};
})();

let scrollAwareSectionsModule = (function() {
	function init(selector) {
		document.querySelectorAll(selector).forEach(function(el) {
			let section = new ScrollAwareSectionModule(el);
			window.addEventListener("scroll", section.onScroll.bind(section));
			window.addEventListener("resize", section.onScroll.bind(section));
		});
	}
	return {
		init: init
	};
})();

scrollAwareSectionsModule.init(".section");
